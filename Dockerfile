FROM maven:3.5.2-jdk-8-alpine AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
RUN mvn package
ENTRYPOINT ["java","-jar","target/workshopmongo-0.0.1-SNAPSHOT.jar"]