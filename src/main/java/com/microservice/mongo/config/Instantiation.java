package com.microservice.mongo.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import com.microservice.mongo.domain.User;
import com.microservice.mongo.repository.UserRepository;

@Configuration
public class Instantiation implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;

	@Override
	public void run(String... args) throws Exception {

		userRepository.deleteAll();

		User maria = new User(null, "Maria Brown", "maria@gmail.com", "Password1");
		User alex = new User(null, "Alex Green", "alex@gmail.com", "Password1");
		User bob = new User(null, "Bob Grey", "bob@gmail.com", "Password1");

		userRepository.saveAll(Arrays.asList(maria, alex, bob));
	}

}
